# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Winter semester 2018
Labs that have been worked on in Graphical and Wireless programming (Android)

* libraries_lab - creating Android library and adding it as a dependency to the app
	* module, project structure, Build Variants (Ctrl+E)

* menus_lab2 - menu with listener
	* OnTouchListener

* menus_lab1 - static menu
	* MenuInflater, onOptionsItemSelected, onCreateOptionsMenu, menu_layout, 

* dynamic_lists2: modified dynamic list 
	* BaseAdapter
	
* dynamic_lists1: dynamic listview
	* POJO, ArrayList, CustomAdapter extending ArrayAdpter<POJO>, setOnItemClickListener, onItemClick, setAdapter

* fragments

* Listview

* Demo 6

* Demo 5

* Demo 4 - passing data (uri)/starting actions with intents
	* putExtra, getIntent, getStringExtra, Uri, Intent.ACTION_VIEW - new Intent(Intent.ACTION_VIEW, uri)
	
* Demo 3 - activity lifecycle
	* onCreate, onStart, onResume, onRestart, onPause, onStop, onDestroy
	* activity states: starting, running, paused, stopped, destroyed	

* Demo 2 - working with intents to switch activities
	* startActivity, Toast, Intent - new Intent(this, activityB.class)

* Demo1 (not present) - onClickListener for simple button